import pytest
import twc.config as config


@pytest.mark.parametrize('key,sequence', [
    ('a', ['a']),
    ('c-a', ['c-a']),
    ('a b', ['a', 'b']),
    ('a c-b', ['a', 'c-b']),
    ('c-a b', ['c-a', 'b']),
    ('space a b c c', ['space', 'a', 'b', 'c', 'c']),
])
def test_bind(key, sequence):
    c = config.Config()

    c.bind(key, 'foobar')

    tch = tuple(sequence)
    assert tch in c.keys
    assert c.keys[tch] == 'foobar'


def test_unbind():
    c = config.Config()

    c.bind('a', 'next task')
    c.bind('b', 'next task')
    assert c.keys[('a',)] == 'next task'
    assert c.keys[('b',)] == 'next task'

    c.unbind('a')
    assert ('a',) not in c.keys
    assert c.keys[('b',)] == 'next task'


def test_rebind():
    c = config.Config()

    c.bind('a', 'next task')
    c.bind('b', 'next task')

    c.bind('a', 'foobar')

    assert c.keys[('a',)] == 'foobar'
    assert c.keys[('b',)] == 'next task'


@pytest.mark.parametrize('name,value,expected', [
    ('timeoutlen', 2500, 2.5),
    ('timeoutlen', ' 2500 ', 2.5),

    ('ttimeoutlen', 2500, 2.5),
    ('ttimeoutlen', ' 2500 ', 2.5),

    ('deffilter', 'abc', 'abc'),
    ('deffilter', ' abc ', ' abc '),

    ('autocomplete', True, True),
    ('autocomplete', False, False),

    ('autohelp', True, True),
    ('autohelp', False, False),

    ('statusleft', ' abc  ', ' abc  '),
    ('statusright', ' abc  ', ' abc  '),

    ('incsearch', True, True),
    ('incsearch', False, False),

    ('ignorecase', True, True),
    ('ignorecase', False, False),

    ('smartcase', True, True),
    ('smartcase', False, False),

    ('agenda', None, None),
    ('agenda', 'abc', 'abc'),
    ('agenda', ' abc ', ' abc '),

    ('taskrc', '~/abc', '~/abc'),
    ('taskrc', '   ~/abc   ', '~/abc'),
])
def test_set(name, value, expected):
    c = config.Config()
    c.set(name, value)
    assert getattr(c.settings, name) == expected

    c1 = config.Config()
    c1.set('  {}   '.format(name), value)
    assert getattr(c1.settings, name) == expected
