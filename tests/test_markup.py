from collections import namedtuple
from datetime import datetime as dt
import pytest

import twc.markup as markup


SUBSTS = {
    'id': 1,
    'foo': 'foo_s',
    'bar': 'bar_s',
    'baz': 'baz_s',
    'empty': '',
    'dt': dt(2016, 8, 13),
    'john': namedtuple('anon', 'doe')('john.doe_s'),  # access by dot
}


@pytest.mark.parametrize('fmt,expected', [
    ('', []),
    (' ', []),
    (' , , + ,', []),
    ('unknown', []),
    ('[unknown]', []),

    # int, short name (previous markup iteration checked min length of name)
    ('id', [('', '1')]),

    ('foo', [('', 'foo_s')]),
    ('[foo]', [('', '[foo_s]')]),
    ('foo:', [('', 'foo_s')]),
    ('foo::', [('', 'foo_s')]),
    ('foo:::', [('', 'foo_s')]),
    ('[foo:]', [('', '[foo_s]')]),
    ('[foo::]', [('', '[foo_s]')]),
    ('[foo:::]', [('', '[foo_s]')]),

    # empty
    ('empty', []),
    ('foo,empty,bar', [('', 'foo_s'), ('', ' '), ('', 'bar_s')]),
    ('foo+empty+bar', [('', 'foo_s'), ('', 'bar_s')]),

    # style
    ('foo:style:', [('class:style', 'foo_s')]),
    ('[foo:style:]', [('class:style', '[foo_s]')]),
    ('[ foo:style: ]', [('class:style', '[ foo_s ]')]),

    # format
    ('[foo::>10:]', [('', '[     foo_s]')]),
    ('foo::>10:', [('', '     foo_s')]),

    # style + format
    ('foo:style:', [('class:style', 'foo_s')]),
    ('foo:style:>10:', [('class:style', '     foo_s')]),
    ('[foo:style:>10:]', [('class:style', '[     foo_s]')]),

    # multi
    ('foo,baz,bar',
     [('', 'foo_s'), ('', ' '), ('', 'baz_s'), ('', ' '), ('', 'bar_s')]),
    ('foo+baz+bar',
     [('', 'foo_s'), ('', 'baz_s'), ('', 'bar_s')]),
    ('foo+[baz]+bar',
     [('', 'foo_s'), ('', '[baz_s]'), ('', 'bar_s')]),
    ('foo,baz:style:,bar',
     [('', 'foo_s'), ('', ' '), ('class:style', 'baz_s'), ('', ' '), ('', 'bar_s')]),
    ('foo,baz:style:>10:,bar',
     [('', 'foo_s'), ('', ' '), ('class:style', '     baz_s'), ('', ' '), ('', 'bar_s')]),
    ('foo,[baz:style:>10:],bar',
     [('', 'foo_s'), ('', ' '), ('class:style', '[     baz_s]'), ('', ' '), ('', 'bar_s')]),

    ('foo,unknown',
     [('', 'foo_s')]),
    ('foo,unknown,bar',
     [('', 'foo_s'), ('', ' '), ('', 'bar_s')]),
    ('foo,[unknown],bar',
     [('', 'foo_s'), ('', ' '), ('', 'bar_s')]),
    ('foo,unknown:white:,bar',
     [('', 'foo_s'), ('', ' '), ('', 'bar_s')]),
    ('foo,[unknown:white:],bar',
     [('', 'foo_s'), ('', ' '), ('', 'bar_s')]),

    # colon inside format string
    ('dt:style:%Y-%m-%d %H:%M:',
     [('class:style', SUBSTS['dt'].strftime('%Y-%m-%d %H:%M'))]),
    ('[dt:style:%Y-%m-%d %H:%M:]',
     [('class:style', '[{}]'.format(SUBSTS['dt'].strftime('%Y-%m-%d %H:%M')))]),

    # user-specified fmtstring end after possible_end
    ('[dt:style:%Y-%m-%d]:',
     [('class:style', '[{}'.format(SUBSTS['dt'].strftime('%Y-%m-%d]')))]),
    ('[dt:style:%Y-%m-%d]:]',
     [('class:style', '[{}]'.format(SUBSTS['dt'].strftime('%Y-%m-%d]')))]),

    # dot inside name (accepted as accessor in format mini-language)
    ('john.doe',
     [('', 'john.doe_s')]),
    ('.. john.doe ..',
     [('', '.. john.doe_s ..')]),
])
def test_markup(fmt, expected):
    p = markup.Parser(SUBSTS)
    p.parse(fmt)
    assert p.markup == expected


@pytest.mark.parametrize('fmt,expected', [
    ('[foo:style]', [('class:style', '[foo_s]')]),
    ('[dt:style:%Y-%m-%d]',
     [('class:style', '[{}]'.format(SUBSTS['dt'].strftime('%Y-%m-%d')))]),
])
def test_markup_fix(fmt, expected):
    p = markup.Parser(SUBSTS)
    p.parse(fmt)
    assert p.markup == expected


@pytest.mark.parametrize('fmt,expected', [
    (':', []),
    ('==', []),

    # colon inside format string, but missing terminating colon
    ('dt:style:%Y-%m-%d %H:%M',
     [('class:style', SUBSTS['dt'].strftime('%Y-%m-%d %H') + '%M')]),
])
def test_markup_nook(fmt, expected):
    p = markup.Parser(SUBSTS)
    p.parse(fmt)
    assert p.markup == expected


@pytest.mark.parametrize('fmt,expected', [
    # colon inside format string, but missing terminating colon
    ('foo:style', [('class:style', 'foo_s')]),
])
def test_markup_fix_mistakes(fmt, expected):
    p = markup.Parser(SUBSTS)
    p.parse(fmt)
    assert p.markup == expected


def test_markup_format():
    formatted = markup.format_('[foo:heading:]', foo='foo_s')
    assert formatted == [('class:heading', '[foo_s]')]


def test_markup_format_map():
    formatted = markup.format_map('[foo:heading:]', {'foo': 'foo_s'})
    assert formatted == [('class:heading', '[foo_s]')]
