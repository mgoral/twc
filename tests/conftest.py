import sys
import os
import attr
import pytest
from unittest.mock import Mock, MagicMock

import twc.config as config


sys.path.append(os.path.join(os.path.dirname(__file__), 'helpers'))


@pytest.fixture
def tw():
    twmock = MagicMock(taskrc_location='taskrc')
    twmock.tasks.return_value = Mock()
    twmock.execute_command = Mock()
    return twmock


@pytest.fixture
def cfg():
    test_config = config.Config()
    return test_config
