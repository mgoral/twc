import copy
import itertools
from collections import defaultdict
import pytest

import twc.twutils as twutils
from twc.task import Task


def perms(*a, **kw):
    return list(itertools.permutations(*a, **kw))


def _gen_tasks():
    tasks = [
        Task(defaultdict(str, uuid='0')),
        Task(defaultdict(str, uuid='1')),
        Task(defaultdict(str, uuid='2')),
        Task(defaultdict(str, uuid='4')),
        Task(defaultdict(str, uuid='5')),
        Task(defaultdict(str, uuid='6')),  # ch of 5 - 3rd level
    ]

    tasks[1].t['depends'] = [tasks[2], tasks[3]]
    tasks[2].t['depends'] = [tasks[4]]
    tasks[4].t['depends'] = [tasks[5]]

    return perms(tasks)


def _find_children_order(tasks, uuids):
    i = 0
    found = []
    for t in tasks:
        if t['uuid'] == str(uuids[i]):
            found.append(t)
            i += 1
    return found


@pytest.fixture(params=_gen_tasks())
def tasks(request):
    return request.param


def test_grouping(tasks):
    tasks = copy.deepcopy(tasks)

    td = {t['uuid']: t for t in tasks}

    grouped = twutils.group_tasks(tasks)

    assert tuple(grouped.keys()) in perms(['0', '1'])

    assert td['0'].depth == 0
    assert td['0'].parent is None
    assert not td['0'].children

    assert td['1'].depth == 0
    assert td['1'].parent is None
    assert tuple(td['1'].children) in perms([td['2'], td['4']])

    assert td['2'].depth == 1
    assert td['2'].parent is td['1']
    assert td['2'].children == [td['5']]

    assert td['4'].depth == 1
    assert td['4'].parent is td['1']
    assert not td['4'].children

    assert td['5'].depth == 2
    assert td['5'].parent is td['2']
    assert td['5'].children == [td['6']]

    assert td['6'].depth == 3
    assert td['6'].parent is td['5']
    assert not td['6'].children
