from twc.twutils import execute_command
from twc.task import Task

from twhelpers import make_task_mock


def test_execute_command(tw):
    tw.execute_command.return_value = ('out', 'err', 0)

    task = Task(make_task_mock(112, auto=True))

    assert execute_command(tw, 'cmd').args == ['cmd']
    assert execute_command(tw, 'cmd with space').args == ['cmd with space']

    assert execute_command(tw, 'cmd', 'foo').args == ['cmd', 'foo']
    assert execute_command(tw, 'cmd', 'foo', ).args == ['cmd', 'foo']
    assert execute_command(tw, 'cmd', 'foo', task).args == ['112', 'cmd', 'foo']
    assert execute_command(tw, 'cmd', 'foo', [task, task]).args == \
        ['112', '112', 'cmd', 'foo']
    assert execute_command(tw, 'cmd', 'foo', '110').args == \
        ['110', 'cmd', 'foo']
    assert execute_command(tw, 'cmd', 'foo bar').args == ['cmd', 'foo', 'bar']
    assert execute_command(tw, 'cmd', ['foo bar']).args == ['cmd', 'foo bar']
    assert execute_command(tw, 'cmd', ['foo', 'bar']).args == \
        ['cmd', 'foo', 'bar']


def test_execute_command_str_escaping(tw):
    tw.execute_command.return_value = ('out', 'err', 0)

    assert execute_command(tw, 'cmd', 'Alice and Bob').args == \
        ['cmd', 'Alice', 'and', 'Bob']
    assert execute_command(tw, 'cmd', "Alice's and Bob's").args == \
        ['cmd', "Alice's", 'and', "Bob's"]
    assert execute_command(tw, 'cmd', "Alice's' and Bob's").args == \
        ['cmd', "Alice's'", 'and', "Bob's"]
    assert execute_command(tw, 'cmd', "Alice 'super slam' and Bob's").args == \
        ['cmd', "Alice", "'super slam'", 'and', "Bob's"]
    assert execute_command(tw, 'cmd', "Alice' and Bob'").args == \
        ['cmd', "Alice'", 'and', "Bob'"]
