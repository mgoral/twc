from unittest.mock import call, ANY

import twc.widgets.completions as completions

from twhelpers import configure_execute_command, make_task_mock


def test_keywords():
    assert completions.keywords()


def test_udas(tw):
    configure_execute_command(tw, stdout=['foo', 'bar', 'baz'])
    compls = completions.udas(tw)
    assert compls == ['foo:', 'bar:', 'baz:']


def test_tags(tw):
    configure_execute_command(tw, stdout=['foo,bar', 'foo', 'bar,baz'])
    compls = completions.tags(tw)

    assert tw.execute_command.call_args_list == \
        [call(['_unique', 'tags'], return_all=ANY, allow_failure=ANY)]
    assert compls == set(['+foo', '+bar', '+baz'])
