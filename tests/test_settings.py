import math
import pytest

from twc.settings import Settings


@pytest.mark.parametrize('name,value,expected', [
    ('timeoutlen', '0', 0),
    ('timeoutlen', '10', 0.01),
    ('timeoutlen', 10, 0.01),
    ('ttimeoutlen', '0', 0),
    ('ttimeoutlen', '10', 0.01),
    ('ttimeoutlen', 10, 0.01),
])
def test_set(name, value, expected):
    s = Settings()
    setattr(s, name, value)

    setting = getattr(s, name)
    if isinstance(expected, float):
        assert isinstance(setting, float)
        assert math.isclose(setting, expected)
    else:
        assert setting == expected


@pytest.mark.parametrize('name,value', [
    ('timeoutlen', None),
    ('timeoutlen', 'abc'),
    ('ttimeoutlen', None),
    ('ttimeoutlen', 'abc'),
])
def test_no_set(name, value):
    s = Settings()
    with pytest.raises(Exception):
        setattr(s, name, value)
