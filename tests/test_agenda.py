import attr
import pytest

from twc.widgets import AgendaView
from twc.widgets.agenda import should_ignore_case, filter_entries
from twc.settings import Settings
from twc.consts import HEADING_MARKER

from twhelpers import configure_tw, make_tasks


def quantify(iterable, pred=bool):
    "Count how many times the predicate is true"
    return sum(map(pred, iterable))


def _test_scroll(agenda, scroll_dir, expected_tasks, expected_headings):
    i = 0
    while agenda.scroll(scroll_dir):
        agenda._get_text_fragments()

        # passed heading adds a newline, so we need to include it for cpos
        # check
        passed_headings = quantify(
            expected_headings, lambda h, pos=agenda.pos: h <= pos)

        exp_pos = expected_tasks[i]
        assert agenda.pos == exp_pos
        assert agenda.cpos == exp_pos + passed_headings

        i += 1
    return i


@pytest.fixture
def prepcfg(cfg):
    cfg.set('deffilter', '')
    return cfg


@pytest.mark.parametrize('exp, text, ign, smart', [
    (True, 'foo', True, True),
    (True, 'foo', True, False),
    (False, 'foo', False, True),
    (False, 'foo', False, False),

    (False, 'FOO', True, True),
    (True, 'FOO', True, False),
    (False, 'FOO', False, True),
    (False, 'FOO', False, False),

    (False, 'Foo', True, True),
    (True, 'Foo', True, False),
    (False, 'Foo', False, True),
    (False, 'Foo', False, False),
])
def test_should_ignore_case(exp, text, ign, smart):
    settings = Settings(ignorecase=ign, smartcase=smart)
    assert exp == should_ignore_case(text, settings)


@pytest.mark.parametrize('filterstr,ignorecase,exp', [
    ('foo', False, [('', 'foo'), HEADING_MARKER]),
    ('Foo', False, [HEADING_MARKER]),
    ('Foo', True, [('', 'foo'), HEADING_MARKER]),
    ('bar', False, [HEADING_MARKER]),
    ('bar', True, [('', 'baR'), HEADING_MARKER]),
    ('baR', False, [('', 'baR'), HEADING_MARKER]),
    ('baR', True, [('', 'baR'), HEADING_MARKER]),

    ('a', False, [('', 'baR'), ('', 'baz'), HEADING_MARKER]),
    ('ba', False, [('', 'baR'), ('', 'baz'), HEADING_MARKER]),

    ('unexpected', False, [HEADING_MARKER]),

    ('', False, [('', 'foo'), ('', 'baR'), ('', 'baz'), HEADING_MARKER]),
    ('', True, [('', 'foo'), ('', 'baR'), ('', 'baz'), HEADING_MARKER]),

    ('!foo', True, [('', 'baR'), ('', 'baz'), HEADING_MARKER]),
    ('!foo', False, [('', 'baR'), ('', 'baz'), HEADING_MARKER]),
    ('!Foo', True, [('', 'baR'), ('', 'baz'), HEADING_MARKER]),
    ('!Foo', False, [('', 'foo'), ('', 'baR'), ('', 'baz'), HEADING_MARKER]),
    ('!bar', False, [('', 'foo'), ('', 'baR'), ('', 'baz'), HEADING_MARKER]),
    ('!bar', True, [('', 'foo'), ('', 'baz'), HEADING_MARKER]),
    ('!baR', False, [('', 'foo'), ('', 'baz'), HEADING_MARKER]),
    ('!baR', True, [('', 'foo'), ('', 'baz'), HEADING_MARKER]),

    ('!a', False, [('', 'foo'), HEADING_MARKER]),
    ('!ba', False, [('', 'foo'), HEADING_MARKER]),
])
def test_filter_entries(filterstr, ignorecase, exp):
    class FakeCacheEntry:
        def __init__(self, entry):
            self.text = [entry]

        def __eq__(self, other):
            return self.text == other.text

        def __repr__(self):
            return str(self.text)

    entries = [
        FakeCacheEntry(('', 'foo')),
        FakeCacheEntry(('', 'baR')),
        FakeCacheEntry(('', 'baz')),
        FakeCacheEntry(HEADING_MARKER),
    ]

    exp = [FakeCacheEntry(e) for e in exp]
    filtered = filter_entries(entries, filterstr, ignorecase)
    assert exp == filtered


def test_agenda_content(tw, prepcfg):
    # 3 untagged tasks
    tasks = make_tasks(1, 4, auto=True)

    # 1 tagged task
    tagged_tasks = make_tasks(4, tags=['foobar'], auto=True)
    tasks.extend(tagged_tasks)

    # 1 tagged task
    tasks.extend(make_tasks(5, auto=True))
    configure_tw(tw, tasks)

    prepcfg.add_block('ag', title='block 1', items='id')
    prepcfg.add_block('ag', title='block 2', items='id', filter='foobar')

    agenda = AgendaView(tw, prepcfg)
    agenda.reset_agenda('ag')

    assert agenda.size == 1 + len(tasks) + 1 + len(tagged_tasks)

    # first block
    assert agenda.is_heading(0)
    for i in range(1, 6):
        agenda.scroll(1)
        assert agenda.pos == i
        assert agenda.is_task(i)
        assert agenda.current_task.t is tasks[i - 1]

    # second block
    agenda.scroll(1)
    assert agenda.is_heading(6)
    assert agenda.is_task(7)
    assert agenda.current_task.t is tasks[3]


def test_scroll_agenda_not_loaded(tw, prepcfg):
    tasks = make_tasks(1, 4, auto=True)
    configure_tw(tw, tasks)

    for i in range(1, 10):
        prepcfg.add_block('ag', title='block', items='id')

    agenda = AgendaView(tw, prepcfg)
    assert not agenda.scroll(1)
    assert agenda.pos == -1

    assert not agenda.scroll(-1)
    assert agenda.pos == -1


def test_scroll_down(tw, prepcfg):
    tasks = make_tasks(1, 4, auto=True)
    configure_tw(tw, tasks)

    for i in range(1, 5):
        prepcfg.add_block('ag', title='block', items='id')

    agenda = AgendaView(tw, prepcfg)
    agenda.reset_agenda('ag')

    # calculate expected positions of headings and tasks
    blocks = prepcfg.agendas['ag'].blocks
    expected_headings = [i * (1 + len(tasks))
                         for i, _ in enumerate(blocks)]
    expected_tasks = [i for i in range(agenda.size)
                      if i not in expected_headings]

    scrolls = _test_scroll(agenda, 1, expected_tasks, expected_headings)
    assert agenda.pos == agenda.size - 1
    assert scrolls == len(blocks) * len(tasks)


def test_scroll_up(tw, prepcfg):
    tasks = make_tasks(1, 4, auto=True)
    configure_tw(tw, tasks)

    for i in range(1, 5):
        prepcfg.add_block('ag', title='block', items='id')

    agenda = AgendaView(tw, prepcfg)
    agenda.reset_agenda('ag')

    blocks = prepcfg.agendas['ag'].blocks
    expected_headings = [i * (1 + len(tasks))
                         for i, _ in enumerate(blocks)]

    # we don't include the first position because AgendaView allows us to
    # initially set position only for its last element, so we'll start from it
    expected_tasks = [i for i in reversed(range(agenda.size))
                      if i not in expected_headings][1:]

    agenda.pos = agenda.size - 1
    assert agenda.pos == agenda.size - 1

    scrolls = _test_scroll(agenda, -1, expected_tasks, expected_headings)
    assert agenda.pos == 1
    assert scrolls > 0


def test_refresh(tw, prepcfg):
    agenda = AgendaView(tw, prepcfg)

    tasks = make_tasks(1, 4, auto=True)
    configure_tw(tw, tasks)
    prepcfg.add_block('ag', title='block', items='id')
    agenda.reset_agenda('ag')

    agenda.refresh()
    assert agenda.pos == -1

    agenda.pos = 2
    assert agenda.pos == 2

    # highlight doesn't change after refresh
    agenda.refresh()
    assert agenda.pos == 2


def test_switch_agenda(tw, prepcfg):
    agenda = AgendaView(tw, prepcfg)

    tasks = make_tasks(1, 4, auto=True)
    tasks.extend(make_tasks(4, tags=['foobar'], auto=True))

    configure_tw(tw, tasks)

    prepcfg.add_block('ag1', title='block', items='id')
    prepcfg.add_block('ag2', title='block', items='id', filter='foobar')

    assert agenda.current_agenda is None

    agenda.reset_agenda('ag1')
    assert agenda.current_agenda.name == 'ag1'
    assert agenda.size > 0
    assert agenda.pos == -1

    agenda.reset_agenda('ag2')
    assert agenda.current_agenda.name == 'ag2'
    assert agenda.size > 0
    assert agenda.pos == -1
    agenda.pos = agenda.size - 1

    agenda.reset_agenda('ag1')
    assert agenda.current_agenda.name == 'ag1'
    assert agenda.pos == -1
    agenda.pos = agenda.size - 1

    agenda.reset_agenda('ag2')
    assert agenda.pos == -1
    agenda.pos = agenda.size - 1
    assert agenda.pos == agenda.size - 1

    agenda.reset_agenda('ag2', keep_pos=True)
    assert agenda.pos == agenda.size - 1

    agenda.reset_agenda('ag1')
    agenda.pos = agenda.size - 1
    agenda.reset_agenda('ag2', keep_pos=True)
    assert agenda.pos > agenda.size
    agenda.scroll(-1)
    assert agenda.pos == agenda.size - 1
