from twc.task import Task


def test_parent_child_relationships():
    p1 = Task(1)
    p2 = Task(2)
    ch1 = Task(3)
    ch2 = Task(4)

    assert p1.children == []
    assert p2.children == []
    assert not ch1.parent
    assert not ch2.parent

    p1.add_child(ch1)
    p1.add_child(ch2)
    assert p1.children == [ch1, ch2]
    assert ch1.parent == p1
    assert ch2.parent == p1

    p2.add_child(ch2)
    assert p1.children == [ch1]
    assert p2.children == [ch2]
    assert ch1.parent == p1
    assert ch2.parent == p2

    ch1.parent = p2
    assert p1.children == []
    assert p2.children == [ch2, ch1]
    assert ch1.parent == p2
    assert ch2.parent == p2

    p2.remove_child(ch1)
    assert p2.children == [ch2]
    assert ch1.parent is None
    assert ch2.parent == p2

    ch2.parent = None
    assert p2.children == []
    assert ch1.parent is None
    assert ch2.parent is None
