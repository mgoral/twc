from collections import OrderedDict
import pytest

import twc.twutils as twutils
from twc.task import Task


@pytest.mark.parametrize('sortcond,expected_order', [
    ('', [0, 1, 2, 3]),
    ('id', [0, 1, 2, 3]),
    ('id-', [3, 2, 1, 0]),
    ('a+', [0, 1, 2, 3]),  # a+ == a-; it is expected, because these do not
    ('a-', [0, 1, 2, 3]),  # reverse values, but rather means __lt__ vs __gt__
    ('c', [1, 0, 2, 3]),
    ('c+', [1, 0, 2, 3]),
    ('c-', [3, 0, 2, 1]),
    ('d+', [1, 2, 0, 3]),
    ('d-', [3, 0, 1, 2]),  # same as TW: tasks without attr are at the bottom
    ('b+,id-', [3, 0, 2, 1]),
    ('b+,id+', [0, 3, 1, 2]),
    ('a+,d-,id-', [3, 0, 2, 1]),
    ('a+,d+,id-', [2, 1, 0, 3]),
])
def test_sort(sortcond, expected_order):
    # Keep ids unique and incremented. They simplify investigation and reporting
    # of errors.
    tl = [
        Task(OrderedDict([('id', 0), ('a', 0), ('b', 0), ('c', 1), ('d', 0)])),
        Task(OrderedDict([('id', 1), ('a', 0), ('b', 1), ('c', 0), ('d', None)])),
        Task(OrderedDict([('id', 2), ('a', 0), ('b', 1), ('c', 1), ('d', None)])),
        Task(OrderedDict([('id', 3), ('a', 0), ('b', 0), ('c', 2), ('d', 1)])),
    ]

    exp = [tl[i] for i in expected_order]

    twutils.sort_tasks(tl, sortcond)
    assert tl == exp, \
        'Order: %s != %s' % (expected_order, [e['id'] for e in tl])
