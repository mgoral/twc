# Copyright (C) 2019 Michał Góral.
#
# This file is part of TWC
#
# TWC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TWC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TWC. If not, see <http://www.gnu.org/licenses/>.

from twc.widgets.agenda import AgendaView
from twc.widgets.commandline import CommandLine
from twc.widgets.tabs import Tabline
from twc.widgets.statusline import StatusLine
from twc.widgets.messagebox import MessageBox
from twc.widgets.text import TextView
from twc.widgets.urls import OpenUrls
